<!-- DOCTYPE -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Viewport Meta Tag -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            AlShams - Social Welfare Society
        </title>
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <!-- Main Style -->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive Style -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
        <!--Fonts-->
        <link rel="stylesheet" media="screen" href="css/fonts/font-awesome/font-awesome.min.css">
        <link rel="stylesheet" media="screen" href="css/fonts/simple-line-icons.css">    
        <link rel="stylesheet" type="text/css" href="css/flexslider.css">
        <!-- Extras -->
        <link rel="stylesheet" type="text/css" href="css/extras/owl/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/extras/owl/owl.theme.css">
        <link rel="stylesheet" type="text/css" href="css/extras/animate.css">
        <link rel="stylesheet" type="text/css" href="css/extras/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/extras/settings.css">

        <!-- Color CSS Styles  -->
        <!--<link rel="stylesheet" type="text/css" href="css/colors/green.css" media="screen" />  -->     
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
        </script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
        </script>
        <![endif]-->
    </head>
    <body>

        <!-- Header area wrapper starts -->
        <header id="header-wrap">

            <!-- Roof area starts -->

            <!-- Roof area Ends -->

            <!-- Header area starts -->
            <section id="header">

                <!-- Navbar Starts -->
                <nav class="navbar navbar-light" data-spy="affix" data-offset-top="50">
                    <div class="container">
                        <button class='navbar-toggler hidden-md-up pull-xs-right' data-target='#main-menu' data-toggle='collapse' type='button'>
                            ☰
                        </button>
                        <!-- Brand -->
                        <a class="navbar-brand" href="index.html">
                            <img src="img/logo.png" alt="" width="160">
                        </a>
                        <div class="collapse navbar-toggleable-sm pull-xs-left pull-md-right" id="main-menu">
                            <!-- Navbar Starts -->
                            <ul class="nav nav-inline">
                                <li class="nav-item dropdown">
                                    <a class="nav-link active" href="index.html" role="button" aria-haspopup="true" aria-expanded="false">Home</a>                  
                                </li>                                     
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        About Us
                                    </a>
                                    <div class="dropdown-menu">                      
                                        <a class="dropdown-item" href="about-us.html">What is SWS?</a>
                                        <a class="dropdown-item" href="about-us2.html">Mission Of SWS</a>
                                        <a class="dropdown-item" href="team-page.html">Funding Sources</a>
                                        <a class="dropdown-item" href="services.html">Committees</a>
                                        <a class="dropdown-item" href="service2.html">Help Domain</a>
                                        <a class="dropdown-item" href="contact1.html">Future Plans</a>
                                    </div>
                                </li>             
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        Projects 
                                    </a>
                                    <div class="dropdown-menu">                      
                                        <a class="dropdown-item" href="accordions.html">Ongoing Projects</a>
                                        <a class="dropdown-item" href="tabs.html">Completed Projects</a>
                                        <a class="dropdown-item" href="buttons.html">Future Projects</a>
                                    </div> 
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="index.html" role="button" aria-haspopup="true" aria-expanded="false">
                                        Structure
                                    </a>                  
                                </li>  
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="index.html" role="button" aria-haspopup="true" aria-expanded="false">
                                        Donations
                                    </a>                  
                                </li>
                                <!-- Search in right of nav -->
                                <li class="nav-item" class="search">
                                    <form class="top_search clearfix">
                                        <div class="top_search_con">
                                            <input class="s" placeholder="Search Here ..." type="text">
                                            <span class="top_search_icon"><i class="icon-magnifier"></i></span>
                                            <input class="top_search_submit" type="submit">
                                        </div>
                                    </form>
                                </li>
                                <!-- Search Ends -->                  
                            </ul>  
                        </div>              
                        <!-- Form for navbar search area -->
                        <form class="full-search">
                            <div class="container">
                                <input type="text" placeholder="Type to Search">
                                <a href="#" class="close-search">
                                    <span class="fa fa-times fa-2x">
                                    </span>
                                </a>
                            </div>
                        </form>
                        <!-- Search form ends -->
                    </div>
                </nav>
                <!-- Navbar Ends -->

            </section> 
            <!-- Start Content -->
            
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <h4>What is AlShams Society</h4>
                        <p>
                            Based in the valley of Malam Jabba, SWAT Pakistan, Al-shams Social Welfare Society, 
                            abbreviated as SWS, is a non-governmental, non-profiting and non-political welfare 
                            organization founded and run by Dr. Rahman Ali (A Social Activist from SWAT) back in 
                            December 16, 2016 with the objectives of promoting education, healthier lifestyle and 
                            welfare services to establish a healthy and aware society.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <section class="slider">
                            <div class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <img src="img/kitchen_adventurer_cheesecake_brownie.jpg" />
                                        <p class="flex-caption">Adventurer Cheesecake Brownie</p>
                                    </li>
                                    <li>
                                        <img src="img/kitchen_adventurer_lemon.jpg" />
                                        <p class="flex-caption">Adventurer Lemon</p>
                                    </li>
                                    <li>
                                        <img src="img/kitchen_adventurer_donut.jpg" />
                                        <p class="flex-caption">Adventurer Donut</p>
                                    </li>
                                    <li>
                                        <img src="img/kitchen_adventurer_caramel.jpg" />
                                        <p class="flex-caption">Adventurer Caramel</p>
                                    </li>
                                </ul>
                            </div>
                        </section>
                    </div>
                    </div>
                        
                    </div>
                         
            <!-- End Content --> 
        </header>
        <!-- Header-wrap Section End -->

        <!-- Service Block-1 Section -->
        <section id="service-block-main" class="section">
            <!-- Container Starts -->
            <div class="container">
                <div class="row">        
                    <h1 class="section-title wow fadeIn animated" data-wow-delay=".2s">
                        SWS MILESTONES
                    </h1>
                    <p class="section-subcontent">
                        To act as a model society to alleviate the suffering of deserving families <br> 
                        through free services.
                    </p>
                    <div class="col-sm-6 col-md-4">
                        <!-- Service-Block-1 Item Starts -->
                        <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".5s">
                            <div class="icon-wrapper text-primary">
                                <i class="icon-book-open pulse-shrink">
                                </i>
                            </div>
                            <h2>
                                Education for All
                            </h2>
                            <p>
                                SWS community believes in Education. We believe everyone has the right to get 
                                access to quality education and serve his family and the nation in a better way. 
                            </p>
                        </div>
                        <!-- Service-Block-1 Item Ends -->
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <!-- Service-Block-1 Item Starts -->
                        <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".8s">
                            <div class="icon-wrapper text-danger">
                                <i class="icon-heart pulse-shrink">
                                </i>
                            </div>
                            <h2>
                                Health is Wealth
                            </h2>
                            <p>
                                SWS Community works for providing free medical assistance to the people to help 
                                in building a healthy society. 
                            </p>
                        </div>
                        <!-- Service-Block-1 Item Ends -->
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <!-- Service-Block-1 Item Starts -->
                        <div class="service-item wow fadeInUpQuick animated" data-wow-delay="1.1s">
                            <div class="icon-wrapper text-success">
                                <i class="icon-users pulse-shrink">
                                </i>
                            </div>
                            <h2>
                                Social Activities & Reforms
                            </h2>
                            <p>
                                We believe in Social unity, co-operation and open mindedness. We aim to resolve 
                                any disputes between the people and to face all the challenges together as a unit. 
                            </p>
                        </div>
                        <!-- Service-Block-1 Item Ends -->
                    </div>

                </div>
            </div><!-- Container Ends -->
        </section><!-- Service Main Section Ends -->

        <!-- Team Section -->
        <section id="team" class="section">
            <!-- Container Starts -->
            <div class="container">        
                <!-- Row Starts -->
                <div class="row">
                    <h1 class="section-title wow fadeInDown" data-wow-delay=".5s">
                        MEET OUR TEAM
                    </h1>
                    <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
                    <div class="col-sm-6 col-md-3">
                        <!-- Team Item Starts -->
                        <div class="team-item wow fadeInUpQuick" data-wow-delay="1s">
                            <figure class="team-profile">
                                <img src="img/team/team-01.jpg" alt="">
                                <figcaption class="our-team">
                                    <div class="details">
                                        <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="orange-line"></div>
                                        <div class="social"> 
                                            <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a> 
                                            <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a> 
                                            <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="info">
                                <h2>
                                    Sara smith
                                </h2>
                                <div class="orange-line"></div>
                                <p>
                                    Founder And ceo
                                </p>
                            </div>
                        </div>
                        <!-- Team Item Ends -->
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <!-- Team Item Starts -->
                        <div class="team-item wow fadeInUpQuick" data-wow-delay="1.4s">
                            <figure class="team-profile">
                                <img src="img/team/team-02.jpg" alt="">
                                <figcaption class="our-team">
                                    <div class="details">
                                        <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="orange-line"></div>
                                        <div class="social"> 
                                            <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a> 
                                            <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a> 
                                            <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="info">
                                <h2>
                                    Sommer Christian
                                </h2>
                                <div class="orange-line"></div>
                                <p>
                                    creative studio head
                                </p>
                            </div>
                        </div><!-- Team Item Starts -->
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <!-- Team Item Starts -->
                        <div class="team-item wow fadeInUpQuick" data-wow-delay="1.8s">
                            <figure class="team-profile">
                                <img src="img/team/team-03.jpg" alt="">
                                <figcaption class="our-team">
                                    <div class="details">
                                        <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="orange-line"></div>
                                        <div class="social"> 
                                            <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a> 
                                            <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a> 
                                            <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="info">
                                <h2>
                                    Jane lupkin
                                </h2>
                                <div class="orange-line"></div>
                                <p>
                                    magento developer
                                </p>
                            </div>
                        </div>
                        <!-- Team Item Ends -->
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <!-- Team Item Starts -->
                        <div class="team-item wow fadeInUpQuick" data-wow-delay="2.2s">
                            <figure class="team-profile">
                                <img src="img/team/team-04.jpg" alt="">
                                <figcaption class="our-team">
                                    <div class="details">
                                        <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="orange-line"></div>
                                        <div class="social"> 
                                            <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a> 
                                            <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a> 
                                            <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="info">
                                <h2>
                                    Sebastian roll
                                </h2>
                                <div class="orange-line"></div>
                                <p>
                                    Logo / branding designer
                                </p>
                            </div>
                        </div><!-- Team Item Ends -->
                    </div>          
                </div><!-- Row Ends -->
            </div><!-- Container Ends -->
        </section>
        <!-- Team Section End -->

        <!-- Testimonial Section -->
        <section id="testimonial" class="section">
            <!-- Container Starts -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="testimonial-item" class="owl-carousel">
                            <div class="item">
                                <div class="testimonial-inner">
                                    <div class="testimonial-images">
                                        <img class="img-circle" src="img/testimonial/img1.jpg" alt="">
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            I am assuming this is the news section. New highlights will flash through
                                            this section. Suppose this is new number one.
                                        </p>
                                    </div>
                                    <div class="testimonial-footer">
                                        <a href="#" class="btn btn-sm btn-info">Details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-inner">
                                    <div class="testimonial-images">
                                        <img class="img-circle" src="img/testimonial/img2.jpg" alt="">
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            I am assuming this is the news section. New highlights will flash through
                                            this section. Suppose this is new number one.
                                        </p>
                                    </div>
                                    <div class="testimonial-footer">
                                        <a href="#" class="btn btn-sm btn-info">Details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-inner">
                                    <div class="testimonial-images">
                                        <img class="img-circle" src="img/testimonial/img3.jpg" alt="">
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            I am assuming this is the news section. New highlights will flash through
                                            this section. Suppose this is new number one.
                                        </p>
                                    </div>
                                    <div class="testimonial-footer">
                                        <a href="#" class="btn btn-sm btn-info">Details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-inner">
                                    <div class="testimonial-images">
                                        <img class="img-circle" src="img/testimonial/img4.jpg" alt="">
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            I am assuming this is the news section. New highlights will flash through
                                            this section. Suppose this is new number one.
                                        </p>
                                    </div>
                                    <div class="testimonial-footer">
                                        <a href="#" class="btn btn-sm btn-info">Details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-inner">
                                    <div class="testimonial-images">
                                        <img class="img-circle" src="img/testimonial/img5.jpg" alt="">
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            I am assuming this is the news section. New highlights will flash through
                                            this section. Suppose this is new number one.
                                        </p>
                                    </div>
                                    <div class="testimonial-footer">
                                        <a href="#" class="btn btn-sm btn-info">Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Container Ends -->
        </section>
        <!-- Testimonial Section End -->                
        

        <!-- Footer Section -->
        <footer>
            <!-- Container Starts -->
            <div class="container">
                <!-- Row Starts -->
                <div class="row section">
                    <!-- Footer Widget Starts -->
                    <div class="footer-widget col-md-6 col-xs-12 wow fadeIn">
                        <h3 class="small-title">
                            FOLLOW US ON SOCIAL NETWORK
                        </h3>
                        <p>
                            Following on our social network will keep you posted with latest, news, and updates. 
                        </p> 
                        
                        <div class="social-footer">
                            <a href="#"><i class="fa fa-facebook icon-round"></i></a>
                            <a href="#"><i class="fa fa-twitter icon-round"></i></a>
                            <a href="#"><i class="fa fa-linkedin icon-round"></i></a>
                            <a href="#"><i class="fa fa-google-plus icon-round"></i></a>
                        </div>           
                    </div><!-- Footer Widget Ends -->

                    <!-- Footer Widget Starts -->
                    <div class="footer-widget col-md-3 col-xs-12 wow fadeIn" data-wow-delay=".5s">
                        <h3 class="small-title">
                            GALLERY
                        </h3>
                        <div class="plain-flicker-gallery">
                            <a href="#" title="Pan Masala"><img src="img/flicker/img1.jpg" alt=""></a>
                            <a href="#" title="Sports Template for Joomla"><img src="img/flicker/img2.jpg" alt=""></a>
                            <a href="" title="Apple Keyboard"><img src="img/flicker/img3.jpg" alt=""></a>
                            <a href="" title="Hard Working"><img src="img/flicker/img4.jpg" alt=""></a>
                            <a href="" title="Smile"><img src="img/flicker/img5.jpg" alt=""></a>
                            <a href="" title="Puzzle"><img src="img/flicker/img6.jpg" alt=""></a>
                        </div>
                    </div><!-- Footer Widget Ends -->

                    <!-- Footer Widget Starts -->
                    <div class="footer-widget col-md-3 col-xs-12 wow fadeIn" data-wow-delay=".8s">
                        <h3 class="small-title">
                            PLACE A MESSAGE
                        </h3>
                        <div class="contact-us">
                            <p>Drops us a message, we will get back to you soon.</p>
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputName2" placeholder="Enter your name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter your email">
                                </div>
                                <button type="submit" class="btn btn-common">Send</button>
                            </form>
                        </div>
                    </div><!-- Footer Widget Ends -->
                </div><!-- Row Ends -->
            </div><!-- Container Ends -->

            <!-- Copyright -->
            <div id="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <p class="copyright-text">
                                &COPY; 2017 - <?php echo date('Y') ?>. All right reserved.
                            </p>
                        </div>
                        <div class="col-md-6  col-sm-6">
                            <ul class="nav nav-inline pull-xs-right">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Sitemap</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Privacy Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Terms of services</a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                </div>
            </div>
            <!-- Copyright  End-->

        </footer>
        <!-- Footer Section End-->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top">
            <i class="fa fa-angle-up">
            </i>
        </a>

        <div class="bottom"> <a href="#" class="settings"></a> </div>

        <!-- JavaScript & jQuery Plugins -->
        <!-- jQuery Load -->
        <script src="js/jquery-min.js"></script>
        <!-- Bootstrap JS -->
        <script src="js/bootstrap.min.js"></script>
        <!--Text Rotator-->
        <script src="js/jquery.mixitup.js"></script>
        <!--WOW Scroll Spy-->
        <script src="js/wow.js"></script>
        <!-- OWL Carousel -->
        <script src="js/owl.carousel.js"></script>
        <!-- WayPoint -->
        <script src="js/waypoints.min.js"></script>
        <!-- CounterUp -->
        <script src="js/jquery.counterup.min.js"></script>
        <!-- ScrollTop -->
        <script src="js/scroll-top.js"></script>
        <!-- Appear -->
        <script src="js/jquery.appear.js"></script>
        <script src="js/jquery.vide.js"></script>
        <!-- All JS plugin Triggers -->
        <script src="js/main.js"></script>
        <!--<script src="js/color-switcher.js"></script>-->
        <script src="js/jquery.flexslider.js"></script>
    </body>
</html>